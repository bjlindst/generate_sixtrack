! parameters (min and max are for 7TeV) (NM)
low_kqt=-5.35e-3;up_kqt=5.35e-3;
low_kq6=-3.85e-3;up_kq6=3.85e-3;
low_kqwa=8e-5;up_kqwa=1.49e-3;
low_kqwb=0;up_kqwb=1.28e-3;
st=1e-5;stw=1e-5;  

scale = 23348.89927*1.05;   ! 23348.89927 is B*rho for 7 TeV. Take 5% margin
from the max strength by matching for a 5% larger rigidity
scmin := 0.03*7000./nrj;
qtlimitx28 := 1.0*225.0/scale;
qtlimitx15 := 1.0*205.0/scale;
qtlimit2 := 1.0*160.0/scale;
qtlimit3 := 1.0*200.0/scale;
qtlimit4 := 1.0*125.0/scale;
qtlimit5 := 1.0*120.0/scale;
qtlimit6 := 1.0*90.0/scale; 



match, sequence=lhcb1,range=s.ds.l7.b1/e.ds.r7.b1, beta0=bir7b1;
    
    vary, name=kqt13.l7b1,  step=1.0E-9, lower=-qtlimit5, upper=qtlimit5;
    vary, name=kqt12.l7b1,  step=1.0E-9, lower=-qtlimit5, upper=qtlimit5;
    vary, name=kqtl11.l7b1, step=1.0E-9, lower=-qtlimit4*300./550., upper=qtlimit4*300./550.;
    vary, name=kqtl10.l7b1, step=1.0E-9, lower=-qtlimit4*500./550., upper=qtlimit4*500./550.;
    vary, name=kqtl9.l7b1,  step=1.0E-9, lower=-qtlimit4*400./550., upper=qtlimit4*400./550.;
    vary, name=kqtl8.l7b1,  step=1.0E-9, lower=-qtlimit4*300./550., upper=qtlimit4*300./550.;
    vary, name=kqtl7.l7b1,  step=1.0E-9, lower=-qtlimit4, upper=qtlimit4;
    vary, name=kq6.l7b1,    step=1.0E-9, lower=-qtlimit6, upper=qtlimit6;
    vary, name=kq6.r7b1,    step=1.0E-9, lower=-qtlimit6, upper=qtlimit6;
    vary, name=kqtl7.r7b1,  step=1.0E-9, lower=-qtlimit4, upper=qtlimit4;
    vary, name=kqtl8.r7b1,  step=1.0E-9, lower=-qtlimit4*550./550., upper=qtlimit4*550./550.;
    vary, name=kqtl9.r7b1,  step=1.0E-9, lower=-qtlimit4*500./550., upper=qtlimit4*500./550.;
    vary, name=kqtl10.r7b1, step=1.0E-9, lower=-qtlimit4, upper=qtlimit4;
    vary, name=kqtl11.r7b1, step=1.0E-9, lower=-qtlimit4, upper=qtlimit4;
    vary, name=kqt12.r7b1,  step=1.0E-9, lower=-qtlimit5, upper=qtlimit5;
    vary, name=kqt13.r7b1,  step=1.0E-9, lower=-qtlimit5, upper=qtlimit5;
    
    ! common quadrupoles for the two beams
    vary,name=kq4.lr7,step=stw,LOWER=low_kqwa,UPPER=up_kqwa;
    vary,name=kq5.lr7,step=stw,LOWER=-up_kqwa,UPPER=-low_kqwa;
    vary,name=kqt4.l7,step=stw,LOWER=low_kqwb,UPPER=up_kqwb;
    vary,name=kqt5.l7,step=stw,LOWER=-up_kqwb,UPPER=-low_kqwb;
    vary,name=kqt4.r7,step=stw,LOWER=low_kqwb,UPPER=up_kqwb;
    vary,name=kqt5.r7,step=stw,LOWER=-up_kqwb,UPPER=-low_kqwb;
    
    constraint, sequence=lhcb1, range=tcp.c6l7.b1,betx>220;
    constraint, sequence=lhcb1, range=tcp.c6l7.b1,bety>220;
    !constraint, sequence=lhcb1, range=tcsg.d4l7.b1,bety>betTCSG.D4L7*0.9;
    !constraint, sequence=lhcb1, range=tcsg.b4l7.b1,betx>betTCSG.b4l7*0.9;
    !constraint, sequence=lhcb1, range=tcsg.6r7.b1,betx>betTCSG.6R7*0.9;
    !constraint, sequence=lhcb1, range=mq.8l7.b1,betx<200;
    !constraint, sequence=lhcb1, range=mqtlh.c6l7.b1,betx>200,bety>300;
    !constraint, sequence=lhcb1, range=mqwa.c5l7.b1,betx>450,bety>500;
    !constraint, sequence=lhcb1, range=mqwa.c4l7.b1,betx>1000,bety>750;



    !constraint, sequence=lhcb1, range=mqwa.c5r7.b1,betx>500,bety>450;

    constraint, sequence=lhcb1, range=e.ds.r7.b1,alfx=eir7b1->alfx,alfy=eir7b1->alfy;
    constraint, sequence=lhcb1, range=e.ds.r7.b1,betx=eir7b1->betx,bety=eir7b1->bety;
    constraint, sequence=lhcb1, range=e.ds.r7.b1,dx=eir7b1->dx,dpx=eir7b1->dpx;
    constraint, sequence=lhcb1, range=e.ds.r7.b1,   mux=muxip7b1+eir7b1->mux;
    constraint, sequence=lhcb1, range=e.ds.r7.b1,   muy=muyip7b1+eir7b1->muy;
    
    !jacobian,calls=jac_calls, tolerance=jac_tol, bisec=jac_bisec;
    !jacobian, calls=300, bisec=6, repeat=3;
    jacobian, tolerance=2.e-21, calls=100;
    !simplex,  calls=15, tolerance=jac_tol;
    !lmdif,calls=200,tolerance=1.e-21;
endmatch;
