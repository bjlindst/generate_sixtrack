import os,sys

#betaStars=[15,20,64,100]
betaStars=[20]
orbitBump=0
tcpBetaRetune=0
offsetAperture=0
fullOutput=1
mbhShift=0
rematchIR7=0

os.system("rm batchLog.txt") 
for betaStar in betaStars:
    if orbitBump == 1:
        str1 = "+ORBITBUMP"
    else:
        str1 = "-ORBITBUMP"
    if tcpBetaRetune == 1:
        str2 = "+TCPBETARETUNE"
    else:
        str2 = "-TCPBETARETUNE"
    if offsetAperture == 1:
        str3 = "+OFFSETAPER"
    else:
        str3 = "-OFFSETAPER"
    if mbhShift == 1:
        str4 = "TCLD"
    else:
        str4 = "NoTCLD"
    if rematchIR7 == 1:
        str5 = "_IR7re"
    else:
        str5 = ""

    if orbitBump+tcpBetaRetune+rematchIR7 == 0:
        run='250muradXing'+str(betaStar)+'cmBeta'+str4+'_default'+str3
    else:
        run='250muradXing'+str(betaStar)+'cmBeta'+str4+str1+str2+str3+str5
    if not os.path.exists('out/'+run):
        os.makedirs('out/'+run)
    if not os.path.exists('out/b1/'+run):
        os.makedirs('out/b1/'+run)    
    if not os.path.exists('out/b4/'+run):
        os.makedirs('out/b4/'+run)  
    if not os.path.exists('temp'):
        os.makedirs('temp')
    os.system('cp jobFiles/* ./')
    os.system("sed -i 's/BETASTARSETTING/"+str(betaStar)+"/g' job_sample_thin_b4.madx") 
    os.system("sed -i 's/BETASTARSETTING/"+str(betaStar)+"/g' job_sample_thin.madx")
    os.system("sed -i 's/ORBITBUMPSWITCH/"+str(orbitBump)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/ORBITBUMPSWITCH/"+str(orbitBump)+"/g' job_sample_thin.madx") 
    os.system("sed -i 's/TCPBETARETUNESWITCH/"+str(tcpBetaRetune)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/TCPBETARETUNESWITCH/"+str(tcpBetaRetune)+"/g' job_sample_thin.madx")   
    os.system("sed -i 's/OFFSETAPERTURESWITCH/"+str(offsetAperture)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/OFFSETAPERTURESWITCH/"+str(offsetAperture)+"/g' job_sample_thin.madx")  
    os.system("sed -i 's/FULLOUTPUTSWITCH/"+str(fullOutput)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/FULLOUTPUTSWITCH/"+str(fullOutput)+"/g' job_sample_thin.madx")    
    os.system("sed -i 's/MBHSHIFT/"+str(mbhShift)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/MBHSHIFT/"+str(mbhShift)+"/g' job_sample_thin.madx")   
    os.system("sed -i 's/REMATCHIR7SWITCH/"+str(rematchIR7)+"/g' job_sample_thin_b4.madx")    
    os.system("sed -i 's/REMATCHIR7SWITCH/"+str(rematchIR7)+"/g' job_sample_thin.madx")  

    os.system("madx job_sample_thin.madx")
    os.system("madx job_sample_thin_b4.madx") 

    os.system("grep IP5 out/LHC* >> batchLog.txt")

    os.system("mv out/LHC* out/"+run)
    os.system("mv out/Surv* out/"+run)
    os.system("mv out/*singlepass* out/"+run)
    os.system("mv out/ap_ir7* out/"+run)
    os.system("mv out/b1/fort* out/b1/"+run)
    os.system("mv out/b4/fort* out/b4/"+run)   


